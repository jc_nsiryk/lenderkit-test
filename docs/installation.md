# Installation

## Installation helpers

Server installation is a complex process with numerous steps to initialize server application. To simplify this process
we use GNU Make utility and configured Makefile's.

There are **2 Makefile's** in the project:

* `/Makefile` or **root level Makefile**.  
This file contains targets to configure your server environment and operate with Docker containers. 
This one should be called **outside** any Docker container (If you're on Mac - inside Vagrant SSH).
* `/src/Makefile` or **application level Makefile**.  
This file contains helpers to operate with PHP application. It should be called **inside PHP containers** (app/queue/scheduler). 
`/src` folder is mounted as a docroot `/var/www/html` to docker php containers, so it's the only available Makefile inside the containers. 
In general this Makefile contains application install/update scripts and `artisan` calls.

## Quick install

To init docker containers and run LenderKit project you can use root level Makefile to do everything for you. 
We will explain all variables below in **Installation in details** section.

#### 0. Clone/Download

* Download project source to some folder
* **Save your license file** as `src/bootstrap/license`. Don't commit this file to git!!!

### 1. Init your environment

After the download, you need to init your project with your own docker-configuration `/.env` file, docker-compose files
 and nginx server config.

To copy all necessary files and get information where you can find your local configs run:

`make init` **OR** `make init-dev` (use `init-dev` to initialize project for development)

By default, **you have all the working configuration files**, and you can leave everything as is. Xdebug and Scheduler
 are turned off by default (you can turn on them later).

After you initialize your environment variables and configure your required ports (inside `.env`, `docker-compose.yml
` and `configs/nginx-server.conf`), you can do a test-run to check that your docker-compose is correct.

### 2. Docker test run

* First you need to do a docker login to the [private docker hub](https://hub.jcdev.net) with your username/password (ask Server Administrator for it).  
    * `docker login hub.jcdev.net:24000` - for node.js generic image
    * `docker login hub.jcdev.net:24200` - for LenderKit official PHP image

* Then you can do a test run.  
`make test-run`

On a test-run:

* Will be generated MySQL database and user
* Queue/Scheduler containers will fail (because we don't have composer/vendors installed)

Wait for "db" container to stop writing output to your console. Last messages should be that it's ready for connection on port 3306.

Now exit your test-run (Ctrl+C) and go to the installation step.

### 3. Installing LenderKit

To install the project after `init` and `test-run` just run:

```bash
make install
```

During the installation process it will ask you to verify your **application `.env` file**. 
You will be able to update App settings, DB settings and Redis Settings if you want. Default values are okay as well.

All other you can configure later.

### 4. Site Domain and Access

On real server you need to configure `configs/nginx-server.conf`, `configs/ssl` for your domains and certificates. 

On localhost you need to point your project domain to a localhost inside `/etc/hosts` file. 
By default, domain is `lenderkit.test`, so you need to add this to your hosts file. Run on your machine _(for Mac users - NOT Vagrant)_:

```bash
sudo bash -c 'echo "127.0.0.1 lenderkit.test" >> /etc/hosts'
``` 

_or just edit this file with editor: `sudo nano /etc/hosts`_

By default, site is accessible within such URLs:

* Admin: https://lenderkit.test:8443
* API: https://lenderkit.test:8888/v1/docs
* Mailhog (test mail server): http://lenderkit.test:8445

If you installed project on localhost you have admin user: admin@gmail.com / admin.

## Start, stop, update

If you reboot your server and don't have auto-start options in your docker-compose config, then you need to launch
docker containers again. To do this just run:

```bash
make run
```

If you want to stop project (update or freeze ports for example) you need another command:

```bash
make stop
```

To update your project to a newer version downloaded from git you can run special command after `git pull` (on **root level**):

```bash
make update
```

## Installation in details ( [#WIP]() )

### Environment Variables

Inside docker `/.env` file you can configure:

* `APP_ID` - can be used inside `docker-compose` files to customize some volumes mapping to project-based specific folders.
* `QUEUE_ENABLED`, `SCHEDULER_ENABLED` - Enable/Disable Queue container (`supervisord` with `artisan queue:work`) and Scheduler container (cron with `artisan schedule:run`)
* `HOST_*` - configuration of external mapped ports for development purposes (like port to connect to MySQL from PHPStorm)
* `SSHD_ENABLED`, `PHP_XDEBUG_ENABLED` - enables SSH server daemon on PHP-FPM container (so you can connect to it via PHPStorm) and enable XDEBUG php extension.
* `PHP_IDE_CONFIG` - contains your server host info, used to find correct Server configuration inside PHPStorm to map files during debug.

### Docker Compose

There are 2 docker-compose main files, which will be initialized:

* `/docker-compose.yml`
* `/docker-compose.override.yml` which is merged automatically by the docker compose utility.  
This file contains a special configuration for development environment (like XDEBUG envs, extra volumes, etc.)


### Installing LenderKit: Under the hood

Installation process in details, what exactly happens inside `make install`:

* First we run the Application make utility inside `app` container (with php-fpm). This utility:
    * Create .env file from example
    * Ask you to modify .env file
    * Chmod storage/bootstrap directories
    * Run `composer install`  
_All composer commands should run with `COMPOSER_MEMORY_LIMIT=2G`, otherwise it will fail._ 
    * Generate new secrets: `php artisan key:generate && php artisan jwt:secret -f`
    * Run migrations and seeds: `php artisan migrate --seed`
    * Symlinking `storage/app/public` folder to `api/public/storage` and `admin/public/storage`
    * Publish lenderkit admin assets, api assets
    * Generates swagger
    * Generate IDE Helper
* After than make will run commands inside `nodejs` container:
    * Run `npm install`, `npm build` inside `admin` folder
_Before these commands you need to switch to JC repository and increase memory limit with such commands:_  
`export NODE_OPTIONS=--max_old_space_size=2048`  
`npm config set registry https://hub.jcdev.net/repository/npm/`
* Stop all containers and network
* Run all containers (here queue and scheduler container should operate as usual)

#### Server configuration

For proper work of the LenderKit based project you have to point apache or nginx to 2 locations one for api `api/public` 
and one for admin panel `admin/public`. Also, you should give a write access to `storage` and `bootstrap/cache
` directories:

So you will have two entry points:
* `admin/public/index.php`
* `api/public/index.php`

**If you run project with docker - this is configured already.**

