# Makefiles reference

As we already mentioned there is 2 Makefiles:

* `/Makefile` or **root level Makefile**.  
This file contains targets to configure your server environment and operate with Docker containers. 
This one should be called **outside** any Docker container (If you're on Mac - inside Vagrant SSH).
* `/src/Makefile` or **application level Makefile**.  
This file contains helpers to operate with PHP application. It should be called **inside PHP containers** (app/queue/scheduler). 
`/src` folder is mounted as a docroot `/var/www/html` to docker php containers, so it's the only available Makefile inside the containers. 
In general this Makefile contains application install/update scripts and `artisan` calls.

## Root level Makefile

### Variables

**NODEJS_MEMORY_LIMIT**  
Specify memory limit for npm build command. For some reason we fail to build application with standard memory limit


### Docker Targets

**info:**  
Print base available commands.

**init:**  
Initialize main config file like `.env`, `docker-compose.yml`, `nginx-server.conf`.

**init-dev:** _depends on `init`_  
Initialize for development. Create `docker-compose.override.yml` file.

**install:**  
Runs full installation process in required containers:
* run php `make install`
* run npm install/npm build

**update:**  
Runs full update process 
* run php `make update`
* run npm install/npm build if not `SKIP_VENDORS` flag specified

**build:**  
Runs docker-compose build

**run:** _depends on `xdebug-init`_  
Write XDebug remote host .env variable and then run containers in daemon mode

**test-run:** _depends on `xdebug-init`_  
Write XDebug remote host .env variable and then run containers without daemon mode

**stop:**  
Stop containers and remove docker network

**xdebug-init:**  
Write XDebug remote host .env variable if we run this script under Vagrant (on MacOS)

**chown:**
Fix file permissions for files created inside docker container.

### PHP container related Targets

**test:**  
Runs phpunit init, run, clean

**phpunit:**  
Runs phpunit with `Core` testsuite

**phpunit-init:**  
Initialize test database to run phpunit tests

**phpunit-cleanup:**  
Drop test database

**php-bash:**  
Login to php `app` container

**php-exec:**  
Ability to run php command w/o logging in to php bash. Usage: `make php-exec COM='php artisan'`

### Node.js Related Targets

**nodejs-bash:**  
Login to node.js container

**npm-i:**  
Run npm install with custom npm repository for admin forms

**npm-build:**  
Run npm run build with node memory limit option

**npm-watch:**  
Run build in watch mode with node memory limit option

## Application (PHP) level Makefile

### Variables

**INSTALLATION_MODE**  
Project or Core development installation mode.

**COMPOSER_MEMORY_LIMIT**  
Composer memory limit option. Increase it in case you have an "allocate memory" fatal error. 

### Main Targets

**info:**  
Shows basic commands list

**install:** _depends on `dotenv-init packages-clear chmod composer-install`_  
Full Laravel/Lenderkit installation process: .env init, chmod, composer install, init app keys, migrations,
publish admin/api assets, generate swagger, ide helpers

**update:** _depends on `packages-clear composer-update cache-clear`_  
Full update process: clean cache, composer, migrations, queue restart, assets publishing, swagger regen.

**composer-install:** _depends on `composer-init`_, _alias: **ci**_  
Runs composer install based on INSTALLATION_MODE with composer memory limit option

**composer-update:** _depends on `composer-init`_, _alias: **cu**_  
Runs composer update based on INSTALLATION_MODE with composer memory limit option

**composer-autoload:** _depends on `composer-init`_, _alias: **ca**_  
Runs composer dump autoload based on INSTALLATION_MODE with composer memory limit option

**composer-init:**  
Creates dev mode `lenderkit/composer.json` in case of dev INSTALLATION_MODE

**chmod:**  
Chmod storage and bootstrap folders

**dotenv-init:**  
Create app .env file and run file edit wizard

**link-public-storage:**  
Add symlinks to public storage

### Helpers

**swagger:**  
Generate swagger cache file

**cache:**  
Cache configs and routes

**cache-clear:** _depends on `packages-clear`_  
Clear everything that can even seems like cache :)

**test:**  
Runs phpunit for testsuite `Core`
