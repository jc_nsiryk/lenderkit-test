<?php

declare(strict_types=1);

use LenderKit\Modules\Themes\Database\Seeds\ThemesTableSeeder;

/**
 * Class DatabaseSeeder
 */
class DatabaseSeeder extends \LenderKit\Database\Seeds\DatabaseSeeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        parent::run();

        $this->seedCore();
        $this->seedCrowdfunding();
        $this->seedModules();

        $this->call(PoliciesSeeder::class);
    }

    /**
     * Seed data from Modules
     */
    protected function seedModules()
    {
        $this->call(ThemesTableSeeder::class);
    }
}
