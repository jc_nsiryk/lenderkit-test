<?php

declare(strict_types=1);

namespace Api\Http;

use LenderKit\Api\Http\Kernel as LenderKitApiKernel;

/**
 * Class Kernel
 *
 * @package Api\Http
 */
class Kernel extends LenderKitApiKernel
{
    /**
     * The project's global HTTP middleware stack.
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $projectMiddleware = [];

    /**
     * The project's route middleware groups.
     *
     * @var array
     */
    protected $projectMiddlewareGroups = [];

    /**
     * The project's route middleware.
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $projectRouteMiddleware = [];
}
