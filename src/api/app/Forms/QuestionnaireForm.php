<?php
declare(strict_types=1);

namespace Api\Forms;

use App\UserConsents\RiskUnderstanding\RiskDifficultToSell;
use App\UserConsents\RiskUnderstanding\RiskIncomeNotGuarantee;
use App\UserConsents\RiskUnderstanding\RiskNotFSCS;
use App\UserConsents\RiskUnderstanding\RiskRepayments;
use App\UserConsents\RiskUnderstanding\RiskUnlistedSecurities;
use Illuminate\Support\Collection;
use LenderKit\Forms\Fields\CheckboxField;
use LenderKit\Models\User;
use LenderKit\Modules\GDPR\Concerns\UserConsentContract;
use LenderKit\Modules\GDPR\Core\UserConsentsManager;

/**
 * Class QuestionnaireForm
 *
 * @package Api\Forms
 */
class QuestionnaireForm extends ApiForm
{
    /**
     * @var Collection
     */
    protected $consents;

    /**
     * Fields
     *
     * @return Collection
     */
    public function fields(): Collection
    {
        return $this->makeConsents()->map(function(UserConsentContract $item) {
            return CheckboxField::make($item->getKey())
                ->value($this->value($item->getKey()))
                ->label($item->getTitle())
                ->attributes(['id' => $item->getKey()])
                ->rules([
                    'required',
                ]);
        });
    }

    /**
     * Rules
     *
     * @return array
     */
    public function rules() : array
    {
        return $this->makeConsents()->mapWithKeys(function(UserConsentContract $item) {
            return [$item->getKey() => 'accepted'];
        })->all();
    }

    /**
     * Form custom messages
     *
     * @return array
     */
    public function messages(): array
    {
        return $this->makeConsents()->mapWithKeys(function(UserConsentContract $item) {
            return [$item->getKey().'.accepted' => 'Risk must be accepted'];
        })->all();
    }

    /**
     * Handle
     *
     */
    public function handle()
    {
        /**
         * @var User $user
         */
        $user = auth()->user();
        /**
         * @var UserConsentsManager $manager
         */
        $manager = app(UserConsentsManager::class);
        foreach ($this->data->all() as $key => $value) {
            $manager->sign($user, $key); // Can be changed to signFormAccepted if needed
        }
    }

    /**
     * Make Consents
     *
     * @return Collection
     */
    protected function makeConsents() : Collection
    {
        if (! $this->consents) {
            $this->consents = collect([
                new RiskIncomeNotGuarantee(),
                new RiskDifficultToSell(),
                new RiskRepayments(),
                new RiskUnlistedSecurities(),
                new RiskNotFSCS(),
            ]);
        }

        return $this->consents;
    }
}
