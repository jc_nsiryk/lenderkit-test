<?php

namespace App\Providers;

use App\UserConsents\AppConsentsRegistrar;
use LenderKit\Modules\GDPR\Core\UserConsentsCollector;
use LenderKit\Providers\AggregateServiceProvider;

/**
 * Class AppServiceProvider
 *
 * @package App\Providers\
 */
class AppServiceProvider extends AggregateServiceProvider
{
    /**
     * Providers
     *
     * @var array
     */
    protected $providers = [
        AuthServiceProvider::class,
        // BroadcastServiceProvider::class,
        EventServiceProvider::class,
    ];

    /**
     * Binders
     *
     * @var array
     */
    protected $binders = [];

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        parent::register();

        $this->bindLenderKitObjects()
            ->registerConsents();
    }

    /**
     * Bind Lender Kit Objects
     *
     * @return AppServiceProvider
     */
    protected function bindLenderKitObjects(): self
    {
        foreach ($this->binders as $binder) {
            (new $binder($this->app))->bind();
        }

        return $this;
    }

    /**
     * Register Consents
     * @return AppServiceProvider
     */
    protected function registerConsents() : AppServiceProvider
    {
        if (app()->provides('gdpr')) {
            UserConsentsCollector::extend(AppConsentsRegistrar::class);
        }

        return $this;
    }
}
