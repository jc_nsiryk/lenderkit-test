<?php
/**
 * You can overwrite LenderKit and Laravel functions here.
 */

declare(strict_types=1);

define('LK_BASE_PATH', realpath(__DIR__ . '/..'));

// Example:
//if (! function_exists('money')) {
//	/**
//	 * Print price with currency
//	 *
//	 * @param float|Money $amount
//	 * @param string $currency
//	 * @param string $decPoint
//	 * @param string $thousandsSep
//	 *
//	 * @return string
//	 */
//	function money($amount, string $currency = null, string $decPoint = '.', string $thousandsSep = ','): string
//	{
//		if ($amount instanceof \LenderKit\Objects\Money) {
//			return $amount->formatted();
//		}
//
//		$currency = $currency ?? config('app.default_currency');
//		$number = ($amount < 0 ? '-' : '')
//			. number_format(round(abs($amount), 2), 2, $decPoint, $thousandsSep);
//
//		if (config('app.money_format') == \LenderKit\Objects\Money::FORMAT_LETTERS) {
//			return "{$number} {$currency}";
//		}
//
//		return currency_icon($currency) . " {$number}";
//	}
//}