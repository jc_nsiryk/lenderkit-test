<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use LenderKit\Console\Kernel as LenderKitConsoleKernel;

/**
 * Class Kernel
 *
 * @package App\Console
 */
class Kernel extends LenderKitConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param Schedule $schedule
     *
     * @return void
     */
    protected function schedule(Schedule $schedule): void
    {
        parent::schedule($schedule);

        $schedule->command('email:send-late-repayments')->dailyAt('9:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands(): void
    {
        parent::commands();

        $this->load(__DIR__ . '/Commands');
    }
}
