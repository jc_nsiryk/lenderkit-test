<?php
declare(strict_types=1);

namespace App\UserConsents;

use App\UserConsents\RiskUnderstanding\BaseRiskUnderstandingConsent;
use App\UserConsents\RiskUnderstanding\RiskDifficultToSell;
use App\UserConsents\RiskUnderstanding\RiskIncomeNotGuarantee;
use App\UserConsents\RiskUnderstanding\RiskNotFSCS;
use App\UserConsents\RiskUnderstanding\RiskRepayments;
use App\UserConsents\RiskUnderstanding\RiskUnlistedSecurities;
use LenderKit\Modules\GDPR\Core\UserConsentsExtender;

/**
 * Class QuestionnaireConsentsExtender
 *
 * @package App\UserConsents
 */
class AppConsentsRegistrar extends UserConsentsExtender
{

    /**
     * Types
     *
     * @return array
     */
    public function types(): array
    {
        return [
           BaseRiskUnderstandingConsent::TYPE_RISK => __('consents.types.risk_understanding'),
        ];
    }

    /**
     * Consents
     *
     * @return string[]
     */
    public function consents(): array
    {
        return [
            RiskDifficultToSell::class,
            RiskIncomeNotGuarantee::class,
            RiskNotFSCS::class,
            RiskRepayments::class,
            RiskUnlistedSecurities::class
        ];
    }
}
