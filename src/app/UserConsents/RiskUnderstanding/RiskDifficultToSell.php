<?php
declare(strict_types=1);

namespace App\UserConsents\RiskUnderstanding;

/**
 * Class RiskDifficultToSell
 *
 * @package App\UserConsents\RiskUnderstanding
 */
class RiskDifficultToSell extends BaseRiskUnderstandingConsent
{
    /**
     * @var string
     */
    protected $key = 'risk_difficult_to_sell';
}
