<?php
declare(strict_types=1);

namespace App\UserConsents\RiskUnderstanding;

use LenderKit\Modules\GDPR\Core\BaseUserConsent;

/**
 * Class BaseRiskUnderstandingConsent
 *
 * @package App\UserConsents\RiskUnderstanding
 */
abstract class BaseRiskUnderstandingConsent extends BaseUserConsent
{
    const TYPE_RISK = 'risk_understanding';

    /**
     * @var string $key
     */
    protected $key;

    /**
     * Get Key
     *
     * @return string
     */
    public function getKey(): string
    {
        return "$this->key";
    }

    /**
     * Get Title
     *
     * @return string
     */
    public function getTitle(): string
    {
        return __('consents.risks.titles.' . $this->key);
    }

}
