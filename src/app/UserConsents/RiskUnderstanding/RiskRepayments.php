<?php
declare(strict_types=1);

namespace App\UserConsents\RiskUnderstanding;

/**
 * Class RiskRepayments
 *
 * @package App\UserConsents\RiskUnderstanding
 */
class RiskRepayments extends BaseRiskUnderstandingConsent
{
    /**
     * @var string
     */
    protected $key = 'risk_repayments';
}
