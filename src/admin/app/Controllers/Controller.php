<?php

declare(strict_types=1);

namespace Admin\Controllers;

use LenderKit\Admin\Controllers\AdminController;

/**
 * Class Controller
 *
 * @package Admin\Controllers
 */
abstract class Controller extends AdminController
{

}
