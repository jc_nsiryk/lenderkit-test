<?php

namespace Admin\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

/**
 * Class RouteServiceProvider
 *
 * @package Admin\Providers
 */
class RouteServiceProvider extends ServiceProvider
{
    /**
     * Admin Namespace
     *
     * @var string
     */
    protected $controllerNamespace = 'Admin\Controllers';

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        Route::middleware(config('admin.middleware'))
            ->namespace($this->controllerNamespace)
            ->as('admin::')
            ->group(base_path('admin/routes/web.php'));
    }
}
