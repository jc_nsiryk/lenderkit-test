/**
 * @copyright Copyright (c) JustCoded Ltd. All Rights Reserved.
 *    Unauthorized copying of this file, via any medium is strictly prohibited.
 *    Proprietary and confidential.
 *
 * @license https://lenderkit.com/license
 * @see https://lenderkit.com/
 *
 * @package LenderKit\Admin
 */

const mix = require('laravel-mix');
let webpack = require('webpack');

mix.webpackConfig({
  resolve: {
    symlinks: false,
  },
  watchOptions: {
    ignored: ["node_modules/**", "app/**", "bootstrap/**", "public/**", "config/**", "routes/**"],
    aggregateTimeout: 0,
    poll: 100,
  }
});

if (!mix.inProduction()) {
  mix.webpackConfig({
    devtool: 'source-map'
  }).sourceMaps()
}

mix
  .setPublicPath('public')
  .options({
    processCssUrls: false,
    clearConsole: true,
    terser: {
      extractComments: false,
    }
  })
  .sass('resources/assets/sass/vendors.scss', 'public/css')
  .sass('resources/assets/sass/app.scss', 'public/css')
  .copy('node_modules/lightbox2/dist/images', 'public/images')
  .js('resources/assets/js/app.js', 'public/js')
  .copy('node_modules/summernote/dist/font', 'public/css/font')
;
