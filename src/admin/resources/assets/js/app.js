require('../vendor/lk-rbam/js/extenders.js'); // RBAM module relation.

require('../vendor/lk-core/js/bootstrap.js');
require('../vendor/lk-core/js/helpers');
require('../vendor/lk-core/js/init');

require('../vendor/lk-reward/js/extenders.js');
// require('../vendor/lk-gdpr/js/extenders.js');

window.Vue.component('ThemeCustomizer', require('../vendor/lk-themes/js/components/Themes/ThemeCustomizer.vue').default); // Themes module relation.

const app = new Vue({
  el: '#app',
});
