<?php
/**
 * @var array $config
 */

declare(strict_types=1);

use LenderKit\Modules\Payments\Gcen\Providers\GcenServiceProvider;
use LenderKit\Modules\Payments\Goji\Providers\GojiServiceProvider;
use LenderKit\Modules\Payments\LemonWay\Providers\LemonWayServiceProvider;
use LenderKit\Modules\Payments\MangoPay\Providers\MangoPayServiceProvider;

$config = require core_config_path() . '/app.php';

// To load only 1 or none payment/kyc service provider in development/testing core/multisite modes
env('MODULE_MANGOPAY_ENABLED', false) && array_push(
    $config['providers'],
    MangoPayServiceProvider::class
);

env('MODULE_LEMONWAY_ENABLED', false) && array_push(
    $config['providers'],
    LemonWayServiceProvider::class
);

env('MODULE_GCEN_ENABLED', false) && array_push(
    $config['providers'],
    GcenServiceProvider::class);

env('MODULE_GOJI_ENABLED', false) && array_push(
    $config['providers'],
    GojiServiceProvider::class
);

$config['providers'] = array_merge($config['providers'], [
    // MUST HAVE LenderKit provider.
    LenderKit\Admin\Providers\AdminServiceProvider::class,

    // TODO: To be moved as separate packages with auto-discover.
    NotificationChannels\Twilio\TwilioProvider::class,

    /*
     * Application Service Providers...
     */
    App\Providers\AppServiceProvider::class,
    Admin\Providers\AdminServiceProvider::class,
]);

$config['aliases']['HtmlHelper'] = LenderKit\Admin\Services\Html\HtmlHelperFacade::class;

return $config;
