<?php
/**
 * @copyright Copyright (c) JustCoded Ltd. All Rights Reserved.
 *    Unauthorized copying of this file, via any medium is strictly prohibited.
 *    Proprietary and confidential.
 *
 * @license https://lenderkit.com/license
 * @see https://lenderkit.com/
 *
 * @package LenderKit\Core
 */

use NotificationChannels\Twilio\TwilioChannel;

return [
	'default'  => env('SMS_CHANNEL', 'twilio'),
	'channels' => [
		'twilio' => [
			'channel' => TwilioChannel::class,
		],
	],
];
