.PHONY: info init init-dev build run test-run install update stop xdebug-init chown npm-i npm-build npm-watch php-bash php-exec nodejs-bash mysql-grants

NODEJS_CONTAINER = nodejs
NODEJS_MEMORY_LIMIT = 2048

info:
	@echo "LenderKit Server Configuration/Launcher"
	@echo " "
	@echo "Usage:"
	@echo "	make command [V=1]"
	@echo " "
	@echo "Options:"
	@echo "	V=1 			Vagrant mode, use it in Vagrant to prevent permission errors"
	@echo "	NONE_INTERACTIVE=1	Use it in CI to prevent errors with docker-compose exec\run"
	@echo " "
	@echo "Available commands:"
	@echo "	init-dev 		Init configurations"
	@echo "	build	 		Build images"
	@echo "	test-run		Docker run in place"
	@echo "	run	 		Docker run as daemon"
	@echo "	stop			Docker stop"
	@echo "	install	 		Run install process (php and nodejs)"
	@echo "	update	 		Run install process (php and nodejs)"
	@echo "	test	 		Run all necessary project tests on running containers"
	@echo "	mysql-grants		Add GRANT OPTION to docker mysql container lenderkit user"
	@echo "	chown	 		Return back correct file owner for files created inside a container"
	@echo "	php-bash		Open php-fpm container bash"
	@echo "	php-exec		Execute bash command inside app php-fpm container"
	@echo "	nodejs-bash		Open nodejs container bash"
	@echo "	npm-i			Install npm modules inside nodejs container (w/o updating package-lock)"
	@echo "	npm-install		Install npm modules inside nodejs container (w/ updating package-lock)"
	@echo "	npm-build		Build admin assets inside nodejs container"
	@echo "	init-watch		Increase system available file watchers"
	@echo "	npm-watch		Admin assets build watcher for development"

CURDIR_BASENAME = $(notdir ${CURDIR})

SHELL = /bin/bash
DOTENV = $(shell cat .env )
DB_DATABASE = $(shell cat .env | grep "^DB_DATABASE=" | sed 's/DB_DATABASE=//')
DB_USERNAME = $(shell cat .env | grep "^DB_USERNAME=" | sed 's/DB_USERNAME=//')
DB_PASSWORD = $(shell cat .env | grep "^DB_PASSWORD=" | sed 's/DB_PASSWORD=//')
DB_DOCKER_CONTAINER = $(shell [[ `docker-compose images` =~ "mysql" ]] && echo '1' || echo '0')

INSTALLATION_MODE = project
DOCKER_COMPOSE_OVERRIDE_SRC = build/docker-compose.dev.yml

ifeq "$(CURDIR_BASENAME)" "boilerplate"
	# mount directory with core and modules exists, adjust composer.json
	INSTALLATION_MODE = dev
	DOCKER_COMPOSE_OVERRIDE_SRC = build/docker-compose.serverdev.yml
endif

# check if we running a vagrant
ifeq "$(USER)" "vagrant"
	V = 1
endif

VAGRANT =
MAYBE_SUDO =
ifneq "$(V)" ""
	VAGRANT = 1
	MAYBE_SUDO = sudo
endif


DOCKER_T_FLAG =
ifeq "$(NON_INTERACTIVE)" "1"
    DOCKER_T_FLAG = -T
endif

DOCKER_COMPOSE_EXEC = docker-compose exec ${DOCKER_T_FLAG} --privileged --index=1
DOCKER_COMPOSE_EXEC_WWW = ${DOCKER_COMPOSE_EXEC} -w /var/www/html
DOCKER_COMPOSE_RUN = docker-compose run ${DOCKER_T_FLAG} -w /var/www/html

############################################
# Make Targets
############################################

init:
	@if [ ! -f '.env' ]; then \
		echo 'Copying .env file...'; \
		${MAYBE_SUDO} cp .env.example .env; \
	fi;
	@if [ ! -f 'docker-compose.yml' ]; then \
		echo 'Copying docker-compose.yml file...'; \
		${MAYBE_SUDO} cp docker-compose.example.yml docker-compose.yml; \
	fi;
	@if [ ! -f 'configs/nginx-server.conf' ]; then \
		echo 'Copying nginx config file...'; \
		${MAYBE_SUDO} cp ./configs/nginx-server.example.conf ./configs/nginx-server.conf; \
	fi;
	@echo ''
	@echo 'NOTE: Please check your configuration in ".env" before run.'
	@echo 'NOTE: Please check your configuration in "docker-compose.yml" before run.'
	@echo 'NOTE: You can update nginx server configuration in "configs/nginx-server.conf".'
	@echo ''

init-dev: init
	@if [ ! -f 'docker-compose.override.yml' ]; then \
		echo 'Copying "docker-compose.override.yml" with dev mode envs...'; \
		${MAYBE_SUDO} cp ${DOCKER_COMPOSE_OVERRIDE_SRC} docker-compose.override.yml; \
	fi;

install: mysql-grants
	docker-compose stop
	${DOCKER_COMPOSE_RUN} app bash -c "make install SKIP_VENDORS=${SKIP_VENDORS}"
	$(MAKE) npm-i
	$(MAKE) npm-build
	${MAYBE_SUDO} touch runtime/installed
	$(MAKE) run V=${VAGRANT}

update:
	docker-compose stop
	${DOCKER_COMPOSE_RUN} app bash -c "make update SKIP_VENDORS=${SKIP_VENDORS}"
	@if [ -z '$(SKIP_VENDORS)' ]; then \
		$(MAKE) npm-i; \
		$(MAKE) npm-build; \
	else \
		echo 'SKIP_VENDORS is active. Skipping npm install and npm build...'; \
	fi
	$(MAKE) run V=${VAGRANT}

build:
	docker-compose build

run: xdebug-init
	docker-compose up --force-recreate --scale nodejs=0 -d

test-run: xdebug-init
	docker-compose up --force-recreate --scale nodejs=0

xdebug-init:
	@if [ ! -z '$(VAGRANT)' ]; then \
		export XDEBUG_REMOTE_HOST=`/sbin/ip route|awk '/default/ { print $$3 }'` \
			&& echo "Set XDEBUG_REMOTE_HOST to $${XDEBUG_REMOTE_HOST} in .env" \
			&& sudo sed -i "s/XDEBUG_REMOTE_HOST=.*/XDEBUG_REMOTE_HOST=$${XDEBUG_REMOTE_HOST}/g" .env; \
	fi

test:
	$(MAKE) phpunit-cleanup
	$(MAKE) phpunit-init
	$(MAKE) phpunit

phpunit:
	$(DOCKER_COMPOSE_EXEC_WWW) app bash -c "make test"

phpunit-init:
	${DOCKER_COMPOSE_EXEC} db bash -c 'mysql -u root -pdeveloper -e "CREATE DATABASE lenderkit_test"'
	${DOCKER_COMPOSE_EXEC} db bash -c "mysql -u root -pdeveloper -e 'GRANT ALL PRIVILEGES ON lenderkit_test.* TO \`${DB_USERNAME}\`@\`%\` IDENTIFIED BY \"${DB_PASSWORD}\"'"
	${DOCKER_COMPOSE_EXEC} db bash -c "mysql -u root -pdeveloper -e 'GRANT GRANT OPTION ON lenderkit_test.* TO \`${DB_USERNAME}\`@\`%\` IDENTIFIED BY \"${DB_PASSWORD}\"'"

phpunit-cleanup:
	${DOCKER_COMPOSE_EXEC} db bash -c 'mysql -u root -pdeveloper -e "DROP DATABASE IF EXISTS lenderkit_test"'

mysql-grants:
	@if [ '1' == '$(DB_DOCKER_CONTAINER)' ]; then \
		echo 'Up DB container'; \
		docker-compose up -d db; \
		echo 'sleep 5s'; \
		sleep 5; \
		echo 'Granting GRANT ALL PRIVILEGES to lenderkit user'; \
		${DOCKER_COMPOSE_EXEC} db bash -c "mysql -u root -pdeveloper -e 'GRANT ALL ON ${DB_DATABASE}.* TO \`${DB_USERNAME}\`@\`%\` IDENTIFIED BY \"${DB_PASSWORD}\"'"; \
		echo 'Granting GRANT OPTION to lenderkit user'; \
		${DOCKER_COMPOSE_EXEC} db bash -c "mysql -u root -pdeveloper -e 'GRANT GRANT OPTION ON ${DB_DATABASE}.* TO \`${DB_USERNAME}\`@\`%\` IDENTIFIED BY \"${DB_PASSWORD}\"'"; \
	fi;

stop:
	docker-compose down

chown:
	sudo chown -R $$(whoami) .env .env.* docker-compose.* src/ configs/

php-bash:
	${DOCKER_COMPOSE_EXEC_WWW} app bash

# TODO: add general command runner inside docker by container name and cmd.
php-exec:
	@if [ ! -z '$(COM)' ]; then \
		echo "${DOCKER_COMPOSE_EXEC_WWW} app bash -c '${COM}'"; \
		${DOCKER_COMPOSE_EXEC_WWW} app bash -c '${COM}'; \
	else \
		echo "COM directive should be set. Example: make php-exec COM='make chmod'"; \
	fi

NODEJS_REPO = npm config set registry https://hub.jcdev.net/repository/npm/
NODEJS_OPTS = export NODE_OPTIONS=--max_old_space_size=$(NODEJS_MEMORY_LIMIT)
NODEJS_BASH_CMD = docker-compose run -w /var/www/html/admin nodejs bash
NODEJS_WORK_DIR = cd /var/www/html/admin &&

nodejs-bash:
	$(NODEJS_BASH_CMD)

npm-i:
	$(NODEJS_BASH_CMD) -c '$(NODEJS_REPO) && npm ci'

npm-install:
	$(NODEJS_BASH_CMD) -c '$(NODEJS_REPO) && npm install'

npm-build:
	$(NODEJS_BASH_CMD) -c '$(NODEJS_OPTS) && npm run build'

npm-watch:
	$(NODEJS_BASH_CMD) -c '$(NODEJS_OPTS) && npm run watch'

init-watch:
	# increase system watchers amount
	echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p
